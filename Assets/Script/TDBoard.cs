﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TDBoard : MonoBehaviour
{
    public TDPiece[,] _TDPieces = new TDPiece[8, 8];
    public Transform _BlackPiecePrefab;
    public Transform _WhitePiecePrefab;
    public LayerMask _BoardLayerMask;
    public UIManager _UIManager;
    public string _BlackWinStr;
    public string _WhiteWinStr;
    public string _DrawStr;
    [HideInInspector]
    public bool _IsGameStarted;

    private Vector3 mBoardOffset = new Vector3(0.04f, -0.04f, 0);
    private Vector3 mPieceOffset = new Vector3(-0.005f, 0.005f, 0);
    private float mOffsetMultiplier = 0.01f;
    private Quaternion mPieceRotation = new Quaternion(0.7f, 0, 0, 0.7f);
    private Vector2 mMouseOverPos, mStartDrag, mEndDrag;
    private TDPiece mSelectedPiece;
    private bool mIsCurrentPlayerWhite = false;
    private List<TDPiece> mForcedPieces;
    private bool mKilled;
    private AIModule mAIModule;
    private Board mLastBoard;

    private void Start()
    {
        GenerateBoard(5);
        mForcedPieces = new List<TDPiece>();
        mAIModule = GetComponent<AIModule>();
    }

    private void Update()
    {
        if (mIsCurrentPlayerWhite)
            return;

        GetMouseOverPosition();

        if (mSelectedPiece != null)
            UpdateSelectedPieceDrag();

        if (Input.GetMouseButtonDown(0))
            SelectPiece();

        if (Input.GetMouseButtonUp(0) && mSelectedPiece != null)
            TryMove();
    }

    public void CreatePiece(eCell piece)
    {
        Transform t = null;
        if (piece == eCell.BLACK)
        {
            t = Instantiate(_BlackPiecePrefab);
            t.localRotation = Quaternion.Euler(90, 0, 180);
        }
        else if (piece == eCell.WHITE)
        {
            t = Instantiate(_WhitePiecePrefab);
            t.localRotation = Quaternion.Euler(90, 0, 180);
        }
        else if (piece == eCell.BLACK_KING)
        {
            t = Instantiate(_BlackPiecePrefab);
            t.GetComponent<TDPiece>()._IsKing = true;
            t.localRotation = Quaternion.Euler(-90, 0, 180);
        }
        else if (piece == eCell.WHITE_KING)
        {
            t = Instantiate(_WhitePiecePrefab);
            t.GetComponent<TDPiece>()._IsKing = true;
            t.localRotation = Quaternion.Euler(-90, 0, 180);
        }

        t.localScale = Vector3.one;
        mSelectedPiece = t.GetComponent<TDPiece>();
        mStartDrag = Vector2.one * -1;
    }

    private void UpdateSelectedPieceDrag()
    {
        if (_IsGameStarted)
        {
            RaycastHit hit;
            if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit, 25.0f, _BoardLayerMask))
                mSelectedPiece.transform.position = hit.point;
        }
        else
        {
            Vector3 pos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            pos.z = 0;
            mSelectedPiece.transform.position = pos;
        }
    }

    private void TryMove()
    {
        ScanForPossibleMove();
        mEndDrag = mMouseOverPos;

        int x1 = (int)mStartDrag.x, y1 = (int)mStartDrag.y, x2 = (int)mEndDrag.x, y2 = (int)mEndDrag.y;

        if (mMouseOverPos.x < 0 || mMouseOverPos.x >= 8 || mMouseOverPos.y < 0 || mMouseOverPos.y >= 8)
        {
            if (!_IsGameStarted)
            {
                DestroyImmediate(mSelectedPiece.gameObject);
                _TDPieces[x1, y1] = null;
            }
            else
            {
                if (mSelectedPiece != null)
                    MovePiece(mSelectedPiece, x1, y1);

                mStartDrag = Vector2.zero;
                mSelectedPiece = null;
                return;
            }
        }

        if (mSelectedPiece != null)
        {
            if (mEndDrag == mStartDrag)
            {
                MovePiece(mSelectedPiece, x1, y1);
                mStartDrag = Vector2.zero;
                mSelectedPiece = null;
            }
            else if (!_IsGameStarted)
            {
                if (mSelectedPiece.transform.parent != transform)
                    mSelectedPiece.transform.parent = transform;

                MovePiece(mSelectedPiece, x2, y2);

                if (x1 >= 0 || y1 >= 0)
                    _TDPieces[x1, y1] = null;

                _TDPieces[x2, y2] = mSelectedPiece;
                mStartDrag = Vector2.zero;
                mEndDrag = Vector2.zero;
                mSelectedPiece = null;
            }
            else if (mSelectedPiece.IsValidMove(_TDPieces, x1, y1, x2, y2))
            {
                if (!mSelectedPiece._IsKing && mSelectedPiece.IsKilledByPiece(_TDPieces, x1, y1, x2, y2))
                {
                    DestroyImmediate(_TDPieces[(x1 + x2) / 2, (y1 + y2) / 2].gameObject);
                    _TDPieces[(x1 + x2) / 2, (y1 + y2) / 2] = null;
                    mKilled = true;
                }
                else if (mSelectedPiece._IsKing && mSelectedPiece.IsKilledByKing(_TDPieces, x1, y1, x2, y2))
                {
                    int initial = -1, final = -1, increment = 0;

                    if (y1 == y2)
                    {
                        initial = x1;
                        final = x2;
                        increment = x1 > x2 ? -1 : 1;

                        do
                        {
                            initial += increment;

                            if (_TDPieces[initial, y1] != null)
                            {
                                DestroyImmediate(_TDPieces[initial, y1].gameObject);
                                _TDPieces[initial, y1] = null;
                                mKilled = true;
                                break;
                            }
                        } while (initial != final);
                    }
                    else if (x1 == x2)
                    {
                        initial = y1;
                        final = y2;
                        increment = y1 > y2 ? -1 : 1;

                        do
                        {
                            initial += increment;

                            if (_TDPieces[x1, initial] != null)
                            {
                                DestroyImmediate(_TDPieces[x1, initial].gameObject);
                                _TDPieces[x1, initial] = null;
                                mKilled = true;
                                break;
                            }
                        } while (initial != final);
                    }
                }

                if (mForcedPieces.Count != 0 && !mKilled)
                {
                    MovePiece(mSelectedPiece, x1, y1);
                    mStartDrag = Vector2.zero;
                    mSelectedPiece = null;
                    return;
                }

                _TDPieces[x1, y1] = null;
                _TDPieces[x2, y2] = mSelectedPiece;
                MovePiece(mSelectedPiece, x2, y2);
                EndTurn();
            }
            else
            {
                MovePiece(mSelectedPiece, x1, y1);
                mStartDrag = Vector2.zero;
                mSelectedPiece = null;
            }
        }
    }

    private void ScanForPossibleMove(TDPiece p, int x, int y)
    {
        mForcedPieces.Clear();
        if (_TDPieces[x, y].IsForcedToMove(_TDPieces, x, y))
            mForcedPieces.Add(_TDPieces[x, y]);
    }

    private void ScanForPossibleMove()
    {
        mForcedPieces.Clear();

        for (int i = 0; i < 8; i++)
            for (int j = 0; j < 8; j++)
                if (_TDPieces[i, j] != null && (mIsCurrentPlayerWhite ? _TDPieces[i, j]._IsWhite : !_TDPieces[i, j]._IsWhite))
                    if (_TDPieces[i, j].IsForcedToMove(_TDPieces, i, j))
                        mForcedPieces.Add(_TDPieces[i, j]);
    }

    private void EndTurn()
    {
        int x = (int)mEndDrag.x, y = (int)mEndDrag.y;

        if ((mSelectedPiece._IsWhite && !mSelectedPiece._IsKing && x == 0) || (!mSelectedPiece._IsWhite && !mSelectedPiece._IsKing && x == 7))
        {
            mSelectedPiece._IsKing = true;
            mSelectedPiece.Flip();
        }

        mSelectedPiece = null;
        mStartDrag = Vector2.zero;
        ScanForPossibleMove(mSelectedPiece, x, y);

        if (mForcedPieces.Count != 0 && mKilled)
        {
            mKilled = false;
            return;
        }

        mKilled = false;
        mIsCurrentPlayerWhite = !mIsCurrentPlayerWhite;
        mSelectedPiece = null;
        CheckVictory();
        if (mIsCurrentPlayerWhite)
            StartCoroutine(MoveAIPlayer());
    }

    private IEnumerator MoveAIPlayer()
    {
        yield return null;
        List<Move> moves = mAIModule.MakeNextMove(_TDPieces, true);

        for (int i = 0; i < moves.Count; i++)
        {
            yield return new WaitForSeconds(2f);
            Move m = moves[i];
            mSelectedPiece = _TDPieces[m.initialRow, m.initialCol];

            int x1 = m.initialRow, y1 = m.initialCol, x2 = m.finalRow, y2 = m.finalCol;

            if (!mSelectedPiece._IsKing && mSelectedPiece.IsKilledByPiece(_TDPieces, x1, y1, x2, y2))
            {
                DestroyImmediate(_TDPieces[(x1 + x2) / 2, (y1 + y2) / 2].gameObject);
                _TDPieces[(x1 + x2) / 2, (y1 + y2) / 2] = null;
            }
            else if (mSelectedPiece._IsKing && mSelectedPiece.IsKilledByKing(_TDPieces, x1, y1, x2, y2))
            {
                int initial = -1, final = -1, increment = 0;

                if (y1 == y2)
                {
                    initial = x1;
                    final = x2;
                    increment = x1 > x2 ? -1 : 1;

                    do
                    {
                        initial += increment;

                        if (_TDPieces[initial, y1] != null)
                        {
                            DestroyImmediate(_TDPieces[initial, y1].gameObject);
                            _TDPieces[initial, y1] = null;
                            break;
                        }
                    } while (initial != final);
                }
                else if (x1 == x2)
                {
                    initial = y1;
                    final = y2;
                    increment = y1 > y2 ? -1 : 1;

                    do
                    {
                        initial += increment;

                        if (_TDPieces[x1, initial] != null)
                        {
                            DestroyImmediate(_TDPieces[x1, initial].gameObject);
                            _TDPieces[x1, initial] = null;
                            break;
                        }
                    } while (initial != final);
                }
            }
            _TDPieces[x1, y1] = null;
            _TDPieces[x2, y2] = mSelectedPiece;
            MovePiece(mSelectedPiece, x2, y2);
            if ((mSelectedPiece._IsWhite && !mSelectedPiece._IsKing && x2 == 0) || (!mSelectedPiece._IsWhite && !mSelectedPiece._IsKing && x2 == 7))
            {
                mSelectedPiece._IsKing = true;
                mSelectedPiece.Flip();
            }
            mSelectedPiece = null;
        }

        mIsCurrentPlayerWhite = !mIsCurrentPlayerWhite;
        mSelectedPiece = null;
        CheckVictory();
        ScanForPossibleMove();
    }

    private void CheckVictory()
    {
        int whiteCount = 0, blackCount = 0;
        for (int i = 0; i < transform.childCount; i++)
        {
            if (transform.GetChild(i).GetComponent<TDPiece>()._IsWhite)
                whiteCount++;
            else
                blackCount++;
        }

        if (whiteCount == 0)
            _UIManager.PromptText(_BlackWinStr);
        else if (blackCount == 0)
            _UIManager.PromptText(_WhiteWinStr);
        else if (blackCount == 1 && whiteCount == 1)
            _UIManager.PromptText(_DrawStr);
    }

    private void SelectPiece()
    {
        _UIManager._PredictionText.text = "";

        if (mMouseOverPos.x < 0 || mMouseOverPos.x >= 8 || mMouseOverPos.y < 0 || mMouseOverPos.y >= 8)
            return;

        int x = (int)mMouseOverPos.x, y = (int)mMouseOverPos.y;

        if (_TDPieces[x, y] != null && (!_IsGameStarted || ((mIsCurrentPlayerWhite && _TDPieces[x, y]._IsWhite) || (!mIsCurrentPlayerWhite && !_TDPieces[x, y]._IsWhite))))
        {
            if (mForcedPieces.Count == 0 && _IsGameStarted)
            {
                mSelectedPiece = _TDPieces[x, y];
                mStartDrag = mMouseOverPos;
            }
            else
            {
                if (_IsGameStarted && mForcedPieces.Find(p => p == _TDPieces[x, y]) == null)
                    return;

                mSelectedPiece = _TDPieces[x, y];
                mStartDrag = mMouseOverPos;

            }
        }
    }

    private void GetMouseOverPosition()
    {
        RaycastHit hit;
        if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit, 25f, _BoardLayerMask))
        {
            mMouseOverPos.x = (int)(hit.point.y + 4f);
            mMouseOverPos.y = (int)(hit.point.x + 4f);
        }
        else
            mMouseOverPos = Vector2.one * -1;
    }

    public void GenerateBoard(int size)
    {
        for (int i = 0; i < 8; i++)
        {
            for (int j = 0; j < 8; j++)
            {
                if (_TDPieces[i, j] != null)
                {
                    DestroyImmediate(_TDPieces[i, j].gameObject);
                    _TDPieces[i, j] = null;
                }
            }
        }

        int offset = (8 - size) / 2;

        for (int j = 0; j < size; j++)
            GeneratePiece(1, offset + j);

        for (int j = 0; j < size; j++)
            GeneratePiece(6, offset + j, true);
    }

    private void GeneratePiece(int x, int y, bool isWhite = false)
    {
        Transform t = null;
        if (isWhite)
            t = Instantiate(_WhitePiecePrefab, transform);
        else
            t = Instantiate(_BlackPiecePrefab, transform);

        MovePiece(t, x, y);
        t.localScale = Vector3.one * mOffsetMultiplier;
        t.localRotation = mPieceRotation;//add random rotation
        _TDPieces[x, y] = t.GetComponent<TDPiece>();
    }

    private void MovePiece(Transform t, int x, int y)
    {
        t.localPosition = Vector3.up * mOffsetMultiplier * x - Vector3.right * mOffsetMultiplier * y + mBoardOffset + mPieceOffset;
    }

    private void MovePiece(TDPiece p, int x, int y)
    {
        p.transform.localPosition = Vector3.up * mOffsetMultiplier * x - Vector3.right * mOffsetMultiplier * y + mBoardOffset + mPieceOffset;
    }

    public int Predict(eTurn turn)
    {
        return mAIModule.Predict(_TDPieces);
    }
}
