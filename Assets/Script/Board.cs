﻿using UnityEngine;
using System;

public enum eCell
{
    NONE = 0,
    WHITE_KING,
    WHITE,
    BLACK_KING,
    BLACK,
}

public enum eTurn
{
    BLACK,
    WHITE,
}

public class Move
{
    public int initialRow;
    public int initialCol;
    public int finalRow;
    public int finalCol;

    public Move(int r1, int c1, int r2, int c2)
    {
        this.initialRow = r1;
        this.initialCol = c1;
        this.finalRow = r2;
        this.finalCol = c2;
    }
}

public class Board
{
    public int _BlackCount;
    public int _WhiteCount;
    public eCell[,] b;

    public Board(TDPiece[,] board)
    {
        b = new eCell[8, 8];

        for(int i = 0; i < 8; i++)
        {
            for(int j = 0; j < 8; j++)
            {
                if (board[i, j] == null)
                    b[i, j] = eCell.NONE;
                else if (board[i, j]._IsWhite)
                {
                    if (board[i, j]._IsKing)
                        b[i, j] = eCell.WHITE_KING;
                    else
                        b[i, j] = eCell.WHITE;

                    _WhiteCount++;
                }
                else
                {
                    if (board[i, j]._IsKing)
                        b[i, j] = eCell.BLACK_KING;
                    else
                        b[i, j] = eCell.BLACK;

                    _BlackCount++;
                }
            }
        }
    }

    public Board(Board board)
    {
        this.b = new eCell[8, 8];
        Array.Copy(board.b, this.b, board.b.Length);
        this._BlackCount = board._BlackCount;
        this._WhiteCount = board._WhiteCount;
    }

    public bool CheckGameComplete()
    {
        return (this._BlackCount == 0 || this._WhiteCount == 0) ? true : false;
    }

    public bool IsWinner(eTurn turn)
    {
        if (turn == eTurn.WHITE && _BlackCount == 0)
            return true;
        else if (turn == eTurn.BLACK && _WhiteCount == 0)
            return true;

        return false;
    }

    public void MakeGenericMove(Move move)
    {
        int x1 = move.initialRow;
        int y1 = move.initialCol;
        int x2 = move.finalRow;
        int y2 = move.finalCol;

        int deltaMoveX = Mathf.Abs(y2 - y1);
        int deltaMoveY = x2 - x1;

        //b[x2, y2] != eCell.NONE has been validated

        if (b[x1,y1] == eCell.BLACK || b[x1,y1] == eCell.WHITE)
        {
            if (deltaMoveX == 1 && deltaMoveY == 0)
                MakeMove(x1, y1, x2, y2);
            else if (deltaMoveX == 0 && (b[x1, y1] == eCell.WHITE ? deltaMoveY == -1 : deltaMoveY == 1))
                MakeMove(x1, y1, x2, y2);
            else if (deltaMoveX == 2 && deltaMoveY == 0 && IsKilledByPiece(x1, y1, x2, y2))
            {
                Capture((x1 + x2) / 2, (y1 + y2) / 2);
                MakeMove(x1, y1, x2, y2);
            }
            else if (deltaMoveX == 0 && (b[x1, y1] == eCell.WHITE ? deltaMoveY == -2 : deltaMoveY == 2) && IsKilledByPiece(x1, y1, x2, y2))
            {
                Capture((x1 + x2) / 2, (y1 + y2) / 2);
                MakeMove(x1, y1, x2, y2);
            }
        }
        else if(b[x1, y1] == eCell.BLACK_KING || b[x1, y1] == eCell.WHITE_KING)
        {
            int initial = -1, final = -1, increment = 0;

            if (y1 == y2)
            {
                initial = x1;
                final = x2;
                increment = x1 > x2 ? -1 : 1;

                do
                {
                    initial += increment;

                    if (b[initial, y1] != eCell.NONE)
                    {
                        Capture(initial, y1);
                        break;
                    }
                } while (initial != final);

                MakeMove(x1, y1, x2, y2);
            }
            else if (x1 == x2)
            {
                initial = y1;
                final = y2;
                increment = y1 > y2 ? -1 : 1;

                do
                {
                    initial += increment;

                    if (b[x1, initial] != eCell.NONE)
                    {
                        Capture(x1, initial);
                        break;
                    }
                } while (initial != final);

                MakeMove(x1, y1, x2, y2);
            }
        }
    }

    public void Capture(int x, int y)
    {
        if (b[x, y] == eCell.WHITE || b[x, y] == eCell.WHITE_KING)
            _WhiteCount--;
        else if (b[x, y] == eCell.BLACK || b[x, y] == eCell.BLACK_KING)
            _BlackCount--;

        b[x, y] = eCell.NONE;
    }

    public bool IsKilledByPiece(int x1, int y1, int x2, int y2)
    {
        int midX = (x1 + x2) / 2;
        int midY = (y1 + y2) / 2;

        if (b[midX, midY] != eCell.NONE)
        {
            if (b[x1, y1] == eCell.WHITE && (b[midX, midY] == eCell.BLACK || b[midX, midY] == eCell.BLACK_KING))
                return true;
            else if (b[x1, y1] == eCell.BLACK && (b[midX, midY] == eCell.WHITE || b[midX, midY] == eCell.WHITE_KING))
                return true;
        }

        return false;
    }

    public void MakeMove(int x1,int y1, int x2, int y2)
    {
        b[x2, y2] = b[x1, y1];
        b[x1, y1] = eCell.NONE;

        if (b[x2, y2] == eCell.WHITE && x2 == 0)
            b[x2, y2] = eCell.WHITE_KING;
        else if (b[x2, y2] == eCell.BLACK && x2 == 7)
            b[x2, y2] = eCell.BLACK_KING;
    }
}
