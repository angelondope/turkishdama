﻿using UnityEngine;

public class TDPiece : MonoBehaviour
{
    public bool _IsWhite;
    public bool _IsKing;

    public TDPiece(bool isKing, bool isWhite)
    {
        _IsKing = isKing;
        _IsWhite = isWhite;
    }

    public TDPiece(TDPiece p)
    {
        _IsKing = p._IsKing;
        _IsWhite = p._IsWhite;
    }

    public bool IsValidMove(TDPiece[,] board, int x1, int y1, int x2, int y2)
    {
        int deltaMoveX = Mathf.Abs(y2 - y1);
        int deltaMoveY = x2 - x1;

        if (board[x2, y2] != null)
            return false;

        if (!_IsKing)
        {
            if (deltaMoveX == 1 && deltaMoveY == 0)
                return true;
            else if (deltaMoveX == 0 && (_IsWhite ? deltaMoveY == -1 : deltaMoveY == 1))
                return true;
            else if (deltaMoveX == 2 && deltaMoveY == 0 && IsKilledByPiece(board, x1, y1, x2, y2))
                return true;
            else if (deltaMoveX == 0 && (_IsWhite ? deltaMoveY == -2 : deltaMoveY == 2) && IsKilledByPiece(board, x1, y1, x2, y2))
                return true;
            else
                return false;
        }
        else if (_IsKing && (IsKingValidMove(board, x1, y1, x2, y2) || IsKilledByKing(board, x1, y1, x2, y2)))
            return true;

        return false;
    }

    public bool IsKilledByPiece(TDPiece[,] board, int x1, int y1, int x2, int y2)
    {
        if (board[(x1 + x2) / 2, (y1 + y2) / 2] != null && ((_IsWhite && !board[(x1 + x2) / 2, (y1 + y2) / 2]._IsWhite) || (!_IsWhite && board[(x1 + x2) / 2, (y1 + y2) / 2]._IsWhite)))
            return true;
        else
            return false;
    }

    public bool IsKingValidMove(TDPiece[,] board, int x1, int y1, int x2, int y2)
    {
        int initial = -1, final = -1, increment = 0;

        if (y1 == y2)
        {
            initial = x1;
            final = x2;
            increment = x1 > x2 ? -1 : 1;

            do
            {
                initial += increment;
                if (board[initial, y1] != null)
                    return false;
            } while (initial != final);

            return true;
        }
        else if (x1 == x2)
        {
            initial = y1;
            final = y2;
            increment = y1 > y2 ? -1 : 1;

            do
            {
                initial += increment;
                if (board[x1,initial] != null)
                    return false;
            } while (initial != final);

            return true;
        }

        return false;
    }

    public bool IsKilledByKing(TDPiece[,] board, int x1, int y1, int x2, int y2)
    {
        int initial = -1, final = -1, increment = 0;

        if (y1 == y2)
        {
            initial = x1;
            final = x2;
            increment = x1 > x2 ? -1 : 1;

            int count = 0;

            do
            {
                initial += increment;

                if (board[initial, y1] != null)
                {
                    if ((board[initial, y1]._IsWhite && _IsWhite) || (!board[initial, y1]._IsWhite && !_IsWhite))
                        return false;

                    if (board[initial, y1]._IsWhite && !_IsWhite)
                        count++;
                    else if (!board[initial, y1]._IsWhite && _IsWhite)
                        count++;
                }
            } while (initial != final);

            if (count == 1)
                return true;
            else
                return false;
        }
        else if (x1 == x2)
        {
            initial = y1;
            final = y2;
            increment = y1 > y2 ? -1 : 1;

            int count = 0;

            do
            {
                initial += increment;

                if (board[x1, initial] != null)
                {
                    if (board[x1, initial]._IsWhite && !_IsWhite)
                        count++;
                    else if (!board[x1, initial]._IsWhite && _IsWhite)
                        count++;
                }
            } while (initial != final);

            if (count == 1)
                return true;
            else
                return false;
        }

        return false;
    }

    public void Flip()
    {
        transform.GetChild(0).GetComponent<Animator>().SetTrigger("FlipTrigger");
    }

    public bool IsForcedToMove(TDPiece[,] board, int x, int y)
    {
        if (!_IsKing)
        {
            if (_IsWhite)
            {
                if (x - 2 >= 0 && board[x - 1, y] != null && !board[x - 1, y]._IsWhite && board[x - 2, y] == null)
                    return true;
                else if (y - 2 >= 0 && board[x, y - 1] != null && !board[x, y - 1]._IsWhite && board[x, y - 2] == null)
                    return true;
                else if (y + 2 <= 7 && board[x, y + 1] != null && !board[x, y + 1]._IsWhite && board[x, y + 2] == null)
                    return true;
                else
                    return false;
            }
            else
            {
                if (x + 2 <= 7 && board[x + 1, y] != null && board[x + 1, y]._IsWhite && board[x + 2, y] == null)
                    return true;
                else if (y - 2 >= 0 && board[x, y - 1] != null && board[x, y - 1]._IsWhite && board[x, y - 2] == null)
                    return true;
                else if (y + 2 <= 7 && board[x, y + 1] != null && board[x, y + 1]._IsWhite && board[x, y + 2] == null)
                    return true;
                else
                    return false;
            }
        }
        else
        {
            if (KingForceKill(board, x, y, 1, 0) || KingForceKill(board, x, y, -1, 0) || KingForceKill(board, x, y, 0, 1) || KingForceKill(board, x, y, 0, -1))
                return true;
            else
                return false;
        }
    }

    private bool KingForceKill(TDPiece[,] board, int x, int y, int incrementX, int incrementY)
    {
        if ((x == 0 && incrementX == -1) || (x == 7 && incrementX == 1) || (y == 0 && incrementY == -1) || (y == 7 && incrementY == 1))
            return false;

        if (board[x, y] != null && board[x + incrementX, y + incrementY] != null && board[x + incrementX, y + incrementY]._IsWhite == board[x, y]._IsWhite)
            return false;
        else if (board[x, y] != null && board[x + incrementX, y + incrementY] == null && ((board[x, y]._IsWhite && !_IsWhite) || (!board[x, y]._IsWhite && _IsWhite)))
            return true;
        else
            return KingForceKill(board, x + incrementX, y + incrementY, incrementX, incrementY);
    }
}
