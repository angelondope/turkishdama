﻿using UnityEngine;
using System.Collections.Generic;

public class AIModule : MonoBehaviour
{
    public int _PointWon = 100000;
    public int _PointKing = 2000;
    public int _PointNormal = 1000;
    public int _PointCenterPiece = 100;
    public int _PointEndPiece = 50;
    public int _PointDefense = 50;
    public int _PointAttackNormal = 1500;
    public int _PointAttackKing = 2500;
    public int _MaxDepth;

    private TDBoard mTDBoard;
    private List<Move> mMoves = new List<Move>();
    private int mPositiveResult = 0;
    private int mNegativeResult = 0;

    private List<Move> CalculateAllForcedMoves(Board board, eTurn turn)
    {
        List<Move> moves = new List<Move>();

        for (int i = 0; i < 8; i++)
        {
            for (int j = 0; j < 8; j++)
            {
                if (board.b[i, j] == eCell.BLACK || board.b[i, j] == eCell.WHITE)
                {
                    if (turn == eTurn.WHITE && board.b[i, j] == eCell.WHITE)
                    {
                        if (i >= 2 && (board.b[i - 1, j] == eCell.BLACK || board.b[i - 1, j] == eCell.BLACK_KING) && board.b[i - 2, j] == eCell.NONE)
                            moves.Add(new Move(i, j, i - 2, j));
                        if (j >= 2 && (board.b[i, j - 1] == eCell.BLACK || board.b[i, j - 1] == eCell.BLACK_KING) && board.b[i, j - 2] == eCell.NONE)
                            moves.Add(new Move(i, j, i, j - 2));
                        if (j <= 5 && (board.b[i, j + 1] == eCell.BLACK || board.b[i, j + 1] == eCell.BLACK_KING) && board.b[i, j + 2] == eCell.NONE)
                            moves.Add(new Move(i, j, i, j + 2));
                    }
                    else if (turn == eTurn.BLACK && board.b[i, j] == eCell.BLACK)
                    {
                        if (i <= 5 && (board.b[i + 1, j] == eCell.WHITE || board.b[i + 1, j] == eCell.WHITE_KING) && board.b[i + 2, j] == eCell.NONE)
                            moves.Add(new Move(i, j, i + 2, j));
                        if (j >= 2 && (board.b[i, j - 1] == eCell.WHITE || board.b[i, j - 1] == eCell.WHITE_KING) && board.b[i, j - 2] == eCell.NONE)
                            moves.Add(new Move(i, j, i, j - 2));
                        if (j <= 5 && (board.b[i, j + 1] == eCell.WHITE || board.b[i, j + 1] == eCell.WHITE_KING) && board.b[i, j + 2] == eCell.NONE)
                            moves.Add(new Move(i, j, i, j + 2));
                    }
                }
                else if (board.b[i, j] == eCell.BLACK_KING || board.b[i, j] == eCell.WHITE_KING)
                {
                    if (i <= 5)
                    {
                        int m = i, n = j;
                        m++;
                        for (; m <= 6; m++)
                        {
                            if (board.b[m, n] == eCell.NONE)
                                continue;
                            else if (turn == eTurn.BLACK && board.b[i, j] == eCell.BLACK_KING && (board.b[m, n] == eCell.WHITE || board.b[m, n] == eCell.WHITE_KING) && board.b[m + 1, n] == eCell.NONE)
                                moves.Add(new Move(i, j, m + 1, n));
                            else if (turn == eTurn.WHITE && board.b[i, j] == eCell.WHITE_KING && (board.b[m, n] == eCell.BLACK || board.b[m, n] == eCell.BLACK_KING) && board.b[m + 1, n] == eCell.NONE)
                                moves.Add(new Move(i, j, m + 1, n));
                            else
                                break;
                        }
                    }

                    if (i >= 2)
                    {
                        int m = i, n = j;
                        m--;
                        for (; m >= 1; m--)
                        {
                            if (board.b[m, n] == eCell.NONE)
                                continue;
                            else if (turn == eTurn.BLACK && board.b[i, j] == eCell.BLACK_KING && (board.b[m, n] == eCell.WHITE || board.b[m, n] == eCell.WHITE_KING) && board.b[m - 1, n] == eCell.NONE)
                                moves.Add(new Move(i, j, m - 1, n));
                            else if (turn == eTurn.WHITE && board.b[i, j] == eCell.WHITE_KING && (board.b[m, n] == eCell.BLACK || board.b[m, n] == eCell.BLACK_KING) && board.b[m - 1, n] == eCell.NONE)
                                moves.Add(new Move(i, j, m - 1, n));
                            else
                                break;
                        }
                    }

                    if (j <= 5)
                    {
                        int m = i, n = j;
                        n++;
                        for (; n <= 6; n++)
                        {
                            if (board.b[m, n] == eCell.NONE)
                                continue;
                            else if (turn == eTurn.BLACK && board.b[i, j] == eCell.BLACK_KING && (board.b[m, n] == eCell.WHITE || board.b[m, n] == eCell.WHITE_KING) && board.b[m, n + 1] == eCell.NONE)
                                moves.Add(new Move(i, j, m, n + 1));
                            else if (turn == eTurn.WHITE && board.b[i, j] == eCell.WHITE_KING && (board.b[m, n] == eCell.BLACK || board.b[m, n] == eCell.BLACK_KING) && board.b[m, n + 1] == eCell.NONE)
                                moves.Add(new Move(i, j, m, n + 1));
                            else
                                break;
                        }
                    }

                    if (j >= 2)
                    {
                        int m = i, n = j;
                        n--;
                        for (; n >= 1; n--)
                        {
                            if (board.b[m, n] == eCell.NONE)
                                continue;
                            else if (turn == eTurn.BLACK && board.b[i, j] == eCell.BLACK_KING && (board.b[m, n] == eCell.WHITE || board.b[m, n] == eCell.WHITE_KING) && board.b[m, n - 1] == eCell.NONE)
                                moves.Add(new Move(i, j, m, n - 1));
                            else if (turn == eTurn.WHITE && board.b[i, j] == eCell.WHITE_KING && (board.b[m, n] == eCell.BLACK || board.b[m, n] == eCell.BLACK_KING) && board.b[m, n - 1] == eCell.NONE)
                                moves.Add(new Move(i, j, m, n - 1));
                            else
                                break;
                        }
                    }
                }
                
            }
        }

        return moves;
    }

    private List<Move> CalculateAllNonForcedMoves(Board board, eTurn turn)
    {
        List<Move> moves = new List<Move>();

        for (int i = 0; i < 8; i++)
        {
            for (int j = 0; j < 8; j++)
            {
                if (board.b[i, j] == eCell.WHITE || board.b[i,j] == eCell.BLACK)
                {
                    if (turn == eTurn.WHITE && board.b[i, j] == eCell.WHITE)
                    {
                        if (i >= 1 && board.b[i - 1, j] == eCell.NONE)
                            moves.Add(new Move(i, j, i - 1, j));
                        if (j <= 6 && board.b[i, j + 1] == eCell.NONE)
                            moves.Add(new Move(i, j, i, j + 1));
                        if (j >= 1 && board.b[i, j - 1] == eCell.NONE)
                            moves.Add(new Move(i, j, i, j - 1));
                    }
                    else if(turn == eTurn.BLACK && board.b[i,j] == eCell.BLACK)
                    {
                        if (i <= 6 && board.b[i + 1, j] == eCell.NONE)
                            moves.Add(new Move(i, j, i + 1, j));
                        if (j <= 6 && board.b[i, j + 1] == eCell.NONE)
                            moves.Add(new Move(i, j, i, j + 1));
                        if (j >= 1 && board.b[i, j - 1] == eCell.NONE)
                            moves.Add(new Move(i, j, i, j - 1));
                    }
                }
                else if (board.b[i, j] == eCell.WHITE_KING || board.b[i, j] == eCell.BLACK_KING)
                {
                    if (i <= 6)
                    {
                        int m = i, n = j;
                        m++;
                        for (; m <= 7; m++)
                        {
                            if (board.b[m, n] == eCell.NONE && ((turn == eTurn.WHITE && board.b[i, j] == eCell.WHITE_KING) || (turn == eTurn.BLACK && board.b[i, j] == eCell.BLACK_KING)))
                                moves.Add(new Move(i, j, m, n));
                            else
                                break;
                        }
                    }

                    if (i >= 1)
                    {
                        int m = i, n = j;
                        m--;
                        for (; m >= 0; m--)
                        {
                            if (board.b[m, n] == eCell.NONE && ((turn == eTurn.WHITE && board.b[i, j] == eCell.WHITE_KING) || (turn == eTurn.BLACK && board.b[i, j] == eCell.BLACK_KING)))
                                moves.Add(new Move(i, j, m, n));
                            else
                                break;
                        }
                    }

                    if (j <= 6)
                    {
                        int m = i, n = j;
                        n++;
                        for (; n <= 7; n++)
                        {
                            if (board.b[m, n] == eCell.NONE && ((turn == eTurn.WHITE && board.b[i, j] == eCell.WHITE_KING) || (turn == eTurn.BLACK && board.b[i, j] == eCell.BLACK_KING)))
                                moves.Add(new Move(i, j, m, n));
                            else
                                break;
                        }
                    }

                    if (j >= 1)
                    {
                        int m = i, n = j;
                        n--;
                        for (; n >= 0; n--)
                        {
                            if (board.b[m, n] == eCell.NONE && ((turn == eTurn.WHITE && board.b[i, j] == eCell.WHITE_KING) || (turn == eTurn.BLACK && board.b[i, j] == eCell.BLACK_KING)))
                                moves.Add(new Move(i, j, m, n));
                            else
                                break;
                        }
                    }
                }
                
            }
        }

        return moves;
    }

    private List<Move> PossibleforcedMove(Board board, int i, int j)
    {
        List<Move> moves = new List<Move>();

        if (board.b[i, j] == eCell.BLACK)
        {
            if (i <= 5 && (board.b[i + 1, j] == eCell.WHITE || board.b[i + 1, j] == eCell.WHITE_KING) && board.b[i + 2, j] == eCell.NONE)
                moves.Add(new Move(i, j, i + 2, j));
            if (j >= 2 && (board.b[i, j - 1] == eCell.WHITE || board.b[i, j - 1] == eCell.WHITE_KING) && board.b[i, j - 2] == eCell.NONE)
                moves.Add(new Move(i, j, i, j - 2));
            if (j <= 5 && (board.b[i, j + 1] == eCell.WHITE || board.b[i, j + 1] == eCell.WHITE_KING) && board.b[i, j + 2] == eCell.NONE)
                moves.Add(new Move(i, j, i, j + 2));
        }
        else if (board.b[i, j] == eCell.WHITE)
        {
            if (i >= 2 && (board.b[i - 1, j] == eCell.BLACK || board.b[i - 1, j] == eCell.BLACK_KING) && board.b[i - 2, j] == eCell.NONE)
                moves.Add(new Move(i, j, i - 2, j));
            if (j >= 2 && (board.b[i, j - 1] == eCell.BLACK || board.b[i, j - 1] == eCell.BLACK_KING) && board.b[i, j - 2] == eCell.NONE)
                moves.Add(new Move(i, j, i, j - 2));
            if (j <= 5 && (board.b[i, j + 1] == eCell.BLACK || board.b[i, j + 1] == eCell.BLACK_KING) && board.b[i, j + 2] == eCell.NONE)
                moves.Add(new Move(i, j, i, j + 2));
        }
        else if(board.b[i, j] == eCell.WHITE_KING || board.b[i, j] == eCell.BLACK_KING)
        {
            if (i <= 5)
            {
                int m = i, n = j;
                m++;
                for (; m <= 6; m++)
                {
                    if (board.b[m, n] == eCell.NONE)
                        continue;
                    else if (board.b[i, j] == eCell.BLACK_KING && (board.b[m, n] == eCell.WHITE || board.b[m, n] == eCell.WHITE_KING) && board.b[m + 1, n] == eCell.NONE)
                        moves.Add(new Move(i, j, m + 1, n));
                    else if (board.b[i, j] == eCell.WHITE_KING && (board.b[m, n] == eCell.BLACK || board.b[m, n] == eCell.BLACK_KING) && board.b[m + 1, n] == eCell.NONE)
                        moves.Add(new Move(i, j, m + 1, n));
                    else
                        break;
                }
            }

            if (i >= 2)
            {
                int m = i, n = j;
                m--;
                for (; m >= 1; m--)
                {
                    if (board.b[m, n] == eCell.NONE)
                        continue;
                    else if (board.b[i, j] == eCell.BLACK_KING && (board.b[m, n] == eCell.WHITE || board.b[m, n] == eCell.WHITE_KING) && board.b[m - 1, n] == eCell.NONE)
                        moves.Add(new Move(i, j, m - 1, n));
                    else if (board.b[i, j] == eCell.WHITE_KING && (board.b[m, n] == eCell.BLACK || board.b[m, n] == eCell.BLACK_KING) && board.b[m - 1, n] == eCell.NONE)
                        moves.Add(new Move(i, j, m - 1, n));
                    else
                        break;
                }
            }

            if (j <= 5)
            {
                int m = i, n = j;
                n++;
                for (; n <= 6; n++)
                {
                    if (board.b[m, n] == eCell.NONE)
                        continue;
                    else if (board.b[i, j] == eCell.BLACK_KING && (board.b[m, n] == eCell.WHITE || board.b[m, n] == eCell.WHITE_KING) && board.b[m, n + 1] == eCell.NONE)
                        moves.Add(new Move(i, j, m, n + 1));
                    else if (board.b[i, j] == eCell.WHITE_KING && (board.b[m, n] == eCell.BLACK || board.b[m, n] == eCell.BLACK_KING) && board.b[m, n + 1] == eCell.NONE)
                        moves.Add(new Move(i, j, m, n + 1));
                    else
                        break;
                }
            }

            if (j >= 2)
            {
                int m = i, n = j;
                n--;
                for (; n >= 1; n--)
                {
                    if (board.b[m, n] == eCell.NONE)
                        continue;
                    else if (board.b[i, j] == eCell.BLACK_KING && (board.b[m, n] == eCell.WHITE || board.b[m, n] == eCell.WHITE_KING) && board.b[m, n - 1] == eCell.NONE)
                        moves.Add(new Move(i, j, m, n - 1));
                    else if (board.b[i, j] == eCell.WHITE_KING && (board.b[m, n] == eCell.BLACK || board.b[m, n] == eCell.BLACK_KING) && board.b[m, n - 1] == eCell.NONE)
                        moves.Add(new Move(i, j, m, n - 1));
                    else
                        break;
                }
            }
        
        }

        return moves;
    }

    private List<Move> ExploreForcedMovesRecursively(Board board, List<List<Move>> sequences, List<Move> moves, int x, int y)
    {
        List<Move> possibleMoves = PossibleforcedMove(board, x, y);

        if(possibleMoves == null || possibleMoves.Count == 0)
        {
            List<Move> copy = new List<Move>(moves);
            sequences.Add(copy);
        }
        else
        {
            for(int i = 0; i < possibleMoves.Count; i++)
            {
                Move m = possibleMoves[i];
                moves.Add(m);
                Board tdBoard = new Board(board);
                tdBoard.MakeGenericMove(m);
                ExploreForcedMovesRecursively(tdBoard, sequences, moves, m.finalRow, m.finalCol);
                moves.Remove(m);
            }
        }

        return possibleMoves;
    }

    public List<List<Move>> ExpandMoves(Board board, eTurn turn)
    {
        List<List<Move>> sequences = new List<List<Move>>();

        List<Move> moves = CalculateAllForcedMoves(board, turn);

        if (moves.Count == 0)
        {
            moves = CalculateAllNonForcedMoves(board, turn);
            for (int i = 0; i < moves.Count; i++)
            {
                List<Move> l = new List<Move>();
                l.Add(moves[i]);
                sequences.Add(l);
            }
        }
        else
        {
            for (int i = 0; i < moves.Count; i++)
            {
                List<Move> l = new List<Move>();
                Move m = moves[i];
                l.Add(m);
                Board tdBoard = new Board(board);
                tdBoard.MakeGenericMove(m);
                ExploreForcedMovesRecursively(tdBoard, sequences, l, m.finalRow, m.finalCol);
                l.Remove(m);
            }
        }

        return sequences;
    }

    private List<Board> GetPossibleBoardConfig(Board board, List<List<Move>> possibleSequences)
    {
        List<Board> possibleBoardConfig = new List<Board>();

        for(int i = 0; i < possibleSequences.Count; i++)
        {
            Board tdBoard = new Board(board);

            for (int j = 0; j < possibleSequences[i].Count; j++)
                tdBoard.MakeGenericMove(possibleSequences[i][j]);

            possibleBoardConfig.Add(tdBoard);
        }

        return possibleBoardConfig;
    }

    private bool CheckGameDraw(Board board, eTurn turn)
    {
        List<List<Move>> possibleMoveSequence = ExpandMoves(board, turn);
        if (possibleMoveSequence == null || possibleMoveSequence.Count == 0)
            return true;
        else
            return false;
    }

    private bool CanExploreFurther(Board board, eTurn turn, int depth)
    {
        if (board.CheckGameComplete() || (board._BlackCount == 1 && board._WhiteCount == 1) || CheckGameDraw(board, turn))
            return false;

        if (depth == _MaxDepth)
            return false;

        return true;
    }

    private int PointDifference(Board board)
    {
        int value = 0;

        for(int i = 0; i < 8; i++)
        {
            for(int j = 0; j < 8; j++)
            {
                if(board.b[i,j] != eCell.NONE)
                {
                    if (board.b[i, j] == eCell.WHITE_KING)
                        value += _PointKing;
                    else if (board.b[i, j] == eCell.BLACK_KING)
                        value += -_PointKing;
                    else if (board.b[i, j] == eCell.WHITE)
                        value += _PointNormal;
                    else if (board.b[i, j] == eCell.BLACK)
                        value += -_PointNormal;
                }
            }
        }

        return value;
    }

    private int EvaluateBoard(Board board, eTurn turn)
    {
        int value = 0;
        if (board.IsWinner(turn))
        {
            value += _PointWon;
            return value;
        }
        else
        {
            value = PointDifference(board);
            value += PointsForAttack(board);
            value /= turn == eTurn.BLACK ? board._WhiteCount : board._BlackCount;
        }
        if (value > 0)
            mPositiveResult += value;
        else
            mNegativeResult += value;

        return value;
    }

    private int AlphaBetaPruning(Board board, eTurn turn, int depth, int alpha, int beta, List<Move> resultMoves)
    {
        if (!CanExploreFurther(board, turn, depth))
            return EvaluateBoard(board, turn);

        List<List<Move>> possibleMoveSequence = ExpandMoves(board, turn);
        List<Board> possibleBoardConfigs = GetPossibleBoardConfig(board, possibleMoveSequence);

        List<Move> bestMoveSequence = null;
        if (turn == eTurn.WHITE)
        {
            for (int i = 0; i < possibleBoardConfigs.Count; i++)
            {
                Board b = possibleBoardConfigs[i];
                List<Move> moveSeq = possibleMoveSequence[i];

                int value = AlphaBetaPruning(b, eTurn.BLACK, depth + 1, alpha, beta, resultMoves);

                if (value > alpha)
                {
                    alpha = value;
                    bestMoveSequence = moveSeq;
                }

                if (alpha > beta)
                    break;
            }

            if (depth == 0 && bestMoveSequence != null)
                resultMoves.AddRange(bestMoveSequence);

            return alpha;
        }
        else
        {
            for (int i = 0; i < possibleBoardConfigs.Count; i++)
            {
                Board b = possibleBoardConfigs[i];
                List<Move> moveSeq = possibleMoveSequence[i];

                int value = AlphaBetaPruning(b, eTurn.WHITE, depth + 1, alpha, beta, resultMoves);

                if (value < beta)
                {
                    beta = value;
                    bestMoveSequence = moveSeq;
                }

                if (alpha > beta)
                    break;
            }

            if (depth == 0 && bestMoveSequence != null)
                resultMoves.AddRange(bestMoveSequence);

            return beta;
        }
    }

    public List<Move> MakeNextMove(TDPiece[,] board, bool isWhite)
    {
        ClearResult();
        Board tdBoard = new Board(board);
        List<Move> resultantMoveSeq = new List<Move>();
        AlphaBetaPruning(tdBoard, isWhite ? eTurn.WHITE : eTurn.BLACK, 0, int.MinValue, int.MaxValue, resultantMoveSeq);
        return resultantMoveSeq;
    }

    public void ClearResult()
    {
        mPositiveResult = 0;
        mNegativeResult = 0;
    }

    public int Predict(TDPiece[,] board)
    {
        ClearResult();
        Board tdBoard = new Board(board);
        List<Move> resultantMoveSeq = new List<Move>();
        AlphaBetaPruning(tdBoard, eTurn.BLACK, 0, int.MinValue, int.MaxValue, resultantMoveSeq);
        float p = ((mNegativeResult * -1f) / ((mPositiveResult - mNegativeResult) * 1f)) * 100f;
        if (p != 100 || (new Board(board))._WhiteCount != 0)
            p = Mathf.Clamp(p, 0, 97f);
        return (int)p;
    }

    private int PointsForAttack(Board board)
    {
        int value = 0;
        for(int i = 0; i < 8; i++)
        {
            for(int j = 0; j < 8; j++)
            {
                if(board.b[i,j] == eCell.BLACK)
                {
                    if (i <= 5 && (board.b[i + 1, j] == eCell.WHITE || board.b[i + 1, j] == eCell.WHITE_KING) && board.b[i + 2, j] == eCell.NONE)
                        value -= board.b[i + 1, j] == eCell.WHITE ?_PointAttackNormal:_PointAttackKing;
                    if (j >= 2 && (board.b[i, j - 1] == eCell.WHITE || board.b[i, j - 1] == eCell.WHITE_KING) && board.b[i, j - 2] == eCell.NONE)
                        value -= board.b[i, j - 1] == eCell.WHITE ? _PointAttackNormal : _PointAttackKing;
                    if (j <= 5 && (board.b[i, j + 1] == eCell.WHITE || board.b[i, j + 1] == eCell.WHITE_KING) && board.b[i, j + 2] == eCell.NONE)
                        value -= board.b[i, j + 1] == eCell.WHITE ? _PointAttackNormal : _PointAttackKing;
                }
                else if (board.b[i, j] == eCell.WHITE)
                {
                    if (i >= 2 && (board.b[i - 1, j] == eCell.BLACK || board.b[i - 1, j] == eCell.BLACK_KING) && board.b[i - 2, j] == eCell.NONE)
                        value += board.b[i - 1, j] == eCell.BLACK ? _PointAttackNormal : _PointAttackKing;
                    if (j >= 2 && (board.b[i, j - 1] == eCell.BLACK || board.b[i, j - 1] == eCell.BLACK_KING) && board.b[i, j - 2] == eCell.NONE)
                        value += board.b[i, j - 1] == eCell.BLACK ? _PointAttackNormal : _PointAttackKing;
                    if (j <= 5 && (board.b[i, j + 1] == eCell.BLACK || board.b[i, j + 1] == eCell.BLACK_KING) && board.b[i, j + 2] == eCell.NONE)
                        value += board.b[i, j + 1] == eCell.BLACK ? _PointAttackNormal : _PointAttackKing;
                }
                else if (board.b[i, j] == eCell.BLACK_KING || board.b[i, j] == eCell.WHITE_KING)
                {
                    if (i <= 5)
                    {
                        int m = i, n = j;
                        m++;
                        for (; m <= 6; m++)
                        {
                            if (board.b[m, n] == eCell.NONE)
                                continue;
                            else if (board.b[i, j] == eCell.BLACK_KING && (board.b[m, n] == eCell.WHITE || board.b[m, n] == eCell.WHITE_KING) && board.b[m + 1, n] == eCell.NONE)
                                value -= board.b[m, n] == eCell.WHITE ? _PointAttackNormal : _PointAttackKing;
                            else if (board.b[i, j] == eCell.WHITE_KING && (board.b[m, n] == eCell.BLACK || board.b[m, n] == eCell.BLACK_KING) && board.b[m + 1, n] == eCell.NONE)
                                value += board.b[m, n] == eCell.BLACK ? _PointAttackNormal : _PointAttackKing;
                            else
                                break;
                        }
                    }

                    if (i >= 2)
                    {
                        int m = i, n = j;
                        m--;
                        for (; m >= 1; m--)
                        {
                            if (board.b[m, n] == eCell.NONE)
                                continue;
                            else if (board.b[i, j] == eCell.BLACK_KING && (board.b[m, n] == eCell.WHITE || board.b[m, n] == eCell.WHITE_KING) && board.b[m - 1, n] == eCell.NONE)
                                value -= board.b[m, n] == eCell.WHITE ? _PointAttackNormal : _PointAttackKing;
                            else if (board.b[i, j] == eCell.WHITE_KING && (board.b[m, n] == eCell.BLACK || board.b[m, n] == eCell.BLACK_KING) && board.b[m - 1, n] == eCell.NONE)
                                value += board.b[m, n] == eCell.BLACK ? _PointAttackNormal : _PointAttackKing;
                            else
                                break;
                        }
                    }

                    if (j <= 5)
                    {
                        int m = i, n = j;
                        n++;
                        for (; n <= 6; n++)
                        {
                            if (board.b[m, n] == eCell.NONE)
                                continue;
                            else if (board.b[i, j] == eCell.BLACK_KING && (board.b[m, n] == eCell.WHITE || board.b[m, n] == eCell.WHITE_KING) && board.b[m, n + 1] == eCell.NONE)
                                value -= board.b[m, n] == eCell.WHITE ? _PointAttackNormal : _PointAttackKing;
                            else if (board.b[i, j] == eCell.WHITE_KING && (board.b[m, n] == eCell.BLACK || board.b[m, n] == eCell.BLACK_KING) && board.b[m, n + 1] == eCell.NONE)
                                value += board.b[m, n] == eCell.BLACK ? _PointAttackNormal : _PointAttackKing;
                            else
                                break;
                        }
                    }

                    if (j >= 2)
                    {
                        int m = i, n = j;
                        n--;
                        for (; n >= 1; n--)
                        {
                            if (board.b[m, n] == eCell.NONE)
                                continue;
                            else if (board.b[i, j] == eCell.BLACK_KING && (board.b[m, n] == eCell.WHITE || board.b[m, n] == eCell.WHITE_KING) && board.b[m, n - 1] == eCell.NONE)
                                value -= board.b[m, n] == eCell.WHITE ? _PointAttackNormal : _PointAttackKing;
                            else if (board.b[i, j] == eCell.WHITE_KING && (board.b[m, n] == eCell.BLACK || board.b[m, n] == eCell.BLACK_KING) && board.b[m, n - 1] == eCell.NONE)
                                value += board.b[m, n] == eCell.BLACK ? _PointAttackNormal : _PointAttackKing;
                            else
                                break;
                        }
                    }
                }
            }
        }
        return value;
    }
}
