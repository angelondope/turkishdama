﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UIManager : MonoBehaviour
{
    public GameObject _GameBoard;
    public GameObject _MenuPanel;
    public GameObject _SetupPanel;
    public GameObject _HUD;
    public TDBoard _TDBoard;
    public float _SetupOrthographicSize;
    public Animator _PromptTextAnimator;
    public Text _PromptText;
    public Text _PredictionText;

    private float mInitialOrthographicsize;

    public void Play()
    {
        _GameBoard.SetActive(true);
        _HUD.SetActive(false);
        _MenuPanel.SetActive(false);
        _SetupPanel.SetActive(true);
        Camera.main.orthographicSize = _SetupOrthographicSize;
    }

    public void CreateNewPiece(int piece)
    {
        _TDBoard.CreatePiece((eCell)piece);
    }

    public void Ready()
    {
        Camera.main.orthographicSize = mInitialOrthographicsize;
        _MenuPanel.SetActive(false);
        _SetupPanel.SetActive(false);
        _HUD.SetActive(true);
        _TDBoard._IsGameStarted = true;
    }

    private void Start()
    {
        mInitialOrthographicsize = Camera.main.orthographicSize;
    }

    public void PromptText(string text)
    {
        _HUD.SetActive(true);
        _PromptText.gameObject.SetActive(true);
        _PromptText.text = text;
        _PromptTextAnimator.SetTrigger("Zoom");
    }

    public void OnAnimationComplete()
    {
        _PromptText.gameObject.SetActive(false);
        Play();
        StartCoroutine(DelayGenerateBoard());
    }

    IEnumerator DelayGenerateBoard()
    {
        yield return null;
        _TDBoard._IsGameStarted = false;
        _TDBoard.GenerateBoard(5);
    }

    public void Predict()
    {
        _PredictionText.text = _TDBoard.Predict(eTurn.BLACK).ToString();
    }
}
